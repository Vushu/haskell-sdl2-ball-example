{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# OPTIONS_GHC -fno-warn-unused-do-bind #-}
{-# OPTIONS_GHC -fno-warn-unused-binds -fno-warn-name-shadowing -fwarn-monomorphism-restriction -fwarn-hi-shadowing #-}
module Main where

import           Control.Lens
import           Control.Monad   (unless, when)
import           Foreign.C.Types
import           GameEntities
import           Linear
import           Linear.Affine   (Point (P))
import           LoadImages
import qualified SDL

main :: IO ()
main = runGame

runGame :: IO()
runGame = initializeGame

screenTick :: CFloat
screenTick = 1000/60 -- we want a frame to take 16.666 milliseconds which is 60 fps

initBall :: SDL.Renderer -> IO Ball
initBall renderer =  do
  tex <- (loadToTexture renderer "ball2.png")
  e <- newEntity (P $ V2 400 (windowHeight-100)) tex (V2 13 (-14)) 100 100
  newBall e $ e^.height/2


initBall2 :: SDL.Renderer -> V2 CFloat -> Point V2 CFloat -> IO Ball
initBall2 renderer vel pos =  do
  tex <- (loadToTexture renderer "ball2.png")
  e <- newEntity pos tex vel 100 100
  newBall e $ e^.height/2

gravityConst :: CFloat
gravityConst = 0.04

initWorld :: SDL.Renderer -> IO World
initWorld renderer = do
  now <- getNow
  b <- initBall renderer
  b2 <- initBall2 renderer (V2 1 (-20)) (P $ V2 0 300)
  b3 <- initBall2 renderer (V2 14 (-9)) (P $ V2 400 300)
  b4 <- initBall2 renderer (V2 14 (-9)) (P $ V2 50 20)
  b5 <- initBall2 renderer (V2 (-14) (-9)) (P $ V2 200 500)
  b6 <- initBall2 renderer (V2 14 (-50)) (P $ V2 600 600)
  tex <- (loadToTexture renderer "bg.png")
  e <- newEntity (P $ V2 0 20) tex (V2 0 0) windowWidth (windowHeight+100)
  newWorld [b,b2,b3,b4,b5,b6] e (V2 0 gravityConst) (now + 700)

initializeGame :: IO ()
initializeGame = do
  SDL.initializeAll
  window <- SDL.createWindow "Ball Example" SDL.defaultWindow
  renderer <- SDL.createRenderer window (-1) SDL.defaultRenderer
  world <- initWorld renderer

  updateGame renderer world
  SDL.quit

clear :: SDL.Renderer -> IO ()
clear renderer = do
  SDL.rendererDrawColor renderer SDL.$= V4 20 110 225 255
  SDL.clear renderer

getNow :: IO CFloat
getNow = do
  ticks <- SDL.ticks
  let now = convertUToCFloat $ CUInt ticks
  --putStrLn . show $ now
  return now

updateGame :: SDL.Renderer -> World -> IO ()
updateGame renderer world = do

  --now <- getNow
  SDL.pollEvents
  clear renderer
  --putStrLn $ ("avg fps:" ++) $ show $ (world^.ticks) / (now - world^.start) * 1000
  w <- collissionDetection =<< animateWorld =<< handleInput =<< updateDelta world
--  when (w^.deltaT > screenTick) $ putStrLn $ "TickTime too slow: " ++ (show $ w^.deltaT)
  drawWorld renderer w
  SDL.present renderer
  when (w^.deltaT < screenTick) $ delayTick (w^.deltaT) --if frame went too fast
  wo <- updateTime w

  unless (wo^.endWorld) (updateGame renderer wo) --forever loop until press esc

delayTick dt = do
  --putStrLn $ "Tick was too fast! " ++ show dt ++ " delay by: " ++ (show $ screenTick - dt)
  SDL.delay $ fromInteger (floatRadix $ screenTick - dt)

handleInput :: World -> IO World
handleInput w =  escPushed =<< spacePushed w

updateTime :: World -> IO World
updateTime world = return $ world & ticks %~ (+ 1)


escPushed :: World -> IO World
escPushed world = do
    keyMap <- SDL.getKeyboardState
    return $ if keyMap SDL.ScancodeEscape then world & endWorld .~ True else world

spacePushed :: World -> IO World
spacePushed world = do
    keyMap <- SDL.getKeyboardState
    if keyMap SDL.ScancodeSpace then jumpBalls world else return world

updateDelta :: World -> IO World
updateDelta world = do
  now <- getNow
  let dt = now - (world^.lastT)
  let w = world & deltaT .~ dt
  return $ w & lastT .~ now
