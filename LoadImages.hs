module LoadImages where

import qualified SDL
import qualified Codec.Picture as J
import qualified Data.Vector.Storable as V
import Linear 

loadImage :: FilePath -> IO (J.Image J.PixelRGBA8)
loadImage path = do
    Right (J.ImageRGBA8 img) <- J.readImage path
    return img

imageToSurface :: J.Image J.PixelRGBA8 -> IO SDL.Surface
imageToSurface (J.Image w h dat) = do
    d <- V.thaw dat
    SDL.createRGBSurfaceFrom
      d
      (fromIntegral <$> V2 w h)
      32
      (fromIntegral w * 4)
      (V4 0x000000FF
          0x0000FF00
          0x00FF0000
          0xFF000000)

newSurface :: FilePath -> IO SDL.Surface
newSurface path = loadImage path >>= imageToSurface

toTexture :: SDL.Renderer -> SDL.Surface -> IO SDL.Texture
toTexture renderer surface = SDL.createTextureFromSurface renderer surface <* SDL.freeSurface surface

loadBmpTexture :: SDL.Renderer -> FilePath -> IO SDL.Texture
loadBmpTexture renderer path = do
  bmp <- SDL.loadBMP path
  SDL.createTextureFromSurface renderer bmp <* SDL.freeSurface bmp

loadToTexture :: SDL.Renderer -> FilePath -> IO SDL.Texture
loadToTexture render fp = toTexture render =<< newSurface fp

