{-# LANGUAGE CPP               #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# OPTIONS_GHC -fno-warn-unused-binds -fno-warn-name-shadowing
    -fwarn-monomorphism-restriction -fwarn-hi-shadowing
 #-}
module GameEntities where

import           Control.Lens
import           Foreign.C.Types
import           Linear
import           Linear.Affine   (Point (P))
import qualified SDL
import           System.Random

windowWidth :: CFloat
windowWidth = 800
windowHeight :: CFloat
windowHeight = 600

data Entity = Entity{
  _position :: Point V2 CFloat
 ,_texture  :: SDL.Texture
 ,_velocity :: V2 CFloat
 ,_width    :: CFloat
 ,_height   :: CFloat
}

data Ball = Ball {
  _entity :: Entity
 ,_radius :: CFloat
}

data World = World {
 _balls       :: [Ball]
 ,_background :: Entity
 ,_endWorld   :: Bool
 ,_gravity    :: V2 CFloat
 ,_lastT      :: CFloat
 ,_deltaT     :: CFloat
 ,_acc60      :: CFloat
 ,_fps        :: CFloat
 ,_ticks      :: CFloat
 ,_start      :: CFloat
}

makeLenses ''Entity
makeLenses ''Ball
makeLenses ''World

{-sdfasdf-}
newEntity :: Point V2 CFloat -> SDL.Texture -> V2 CFloat -> CFloat -> CFloat -> IO Entity
newEntity pos tex velo w h =
  return Entity {
   _position = pos
  ,_texture = tex
  ,_velocity = velo
  ,_width = w
  ,_height = h
}

newBall :: Entity -> CFloat -> IO Ball
newBall ent r =
  return Ball {_entity = ent, _radius = r}

--madgadfsa
newWorld :: [Ball] -> Entity -> V2 CFloat -> CFloat -> IO World
newWorld b back grav st=
  return World {
   _balls = b
  ,_background = back
  ,_endWorld = False
  ,_gravity = grav
  ,_lastT = 0
  ,_deltaT = 0
  ,_acc60 = 0
  ,_fps = 0
  ,_ticks = 0
  ,_start = st
}

friction :: CFloat
friction = 0.9

{- Draw Functions-}

drawWorld :: SDL.Renderer -> World -> IO ()
drawWorld renderer world = do

  drawEntity renderer (world^.background)
  mapM_ (drawBall renderer) (world^.balls)

drawBall :: SDL.Renderer -> Ball -> IO ()
drawBall renderer b = drawEntity renderer (b^.entity)

drawEntity :: SDL.Renderer ->  Entity -> IO ()
drawEntity renderer e = drawTexture renderer (e^.texture) (e^.position) (e^.width) (e^.height)

drawTexture :: SDL.Renderer -> SDL.Texture -> Point V2 CFloat -> CFloat -> CFloat -> IO ()
drawTexture renderer tex pos w h= do
  let vectorWH = V2 (convertToCInt w) (convertToCInt h)
  SDL.copy renderer tex Nothing (Just $ SDL.Rectangle (vectorToCInt pos) vectorWH)

vectorToCInt :: Point V2 CFloat -> Point V2 CInt
vectorToCInt (P (V2 a b)) = P $ V2 (convertToCInt a) (convertToCInt b)
convertToCFloat :: CInt -> CFloat
convertToCFloat cint = fromRational . toRational $ cint

jumpBalls :: World -> IO World
jumpBalls wo = do
  --let jumpVector = (V2 r (-10))
  bals <- mapM (updateBallRandomVelocity) (wo^.balls)
  newPos <- mapM (newPosition (wo^.deltaT)) bals
  e2 <- mapM (updatePos) (bals `zip` newPos)
  bb <- mapM (updateBall) (bals `zip` e2)
  return $ wo & balls .~ bb


convertToCInt :: CFloat -> CInt
convertToCInt cfloat = fromInteger . fromIntegral $ fromEnum cfloat

convertUToCFloat :: CUInt -> CFloat
convertUToCFloat cint = fromRational . toRational $ cint

animateWorld :: World -> IO World
animateWorld w = do
  b <- (animateBall (w^.deltaT) (w^.gravity)) (w^.balls)
  return $ w & balls .~ b

animateBall :: CFloat -> V2 CFloat -> [Ball] -> IO [Ball]
animateBall dt grav bl = do
  bals <- mapM (updateBallVelocity grav) bl
  newPos <- mapM (newPosition dt) bals
  e2 <- mapM (updatePos) (bals `zip` newPos)
  mapM (updateBall) (bals `zip` e2)

updateBall :: (Ball,Entity) -> IO Ball
updateBall (b,e) = do
  return (b & entity .~ e)

updatePos :: (Ball,Point V2 CFloat) -> IO Entity
updatePos (b,newPos) = do
  return $ b^.entity & position .~ newPos

newPosition :: CFloat -> Ball -> IO (Point V2 CFloat)
newPosition dt b = do
  return $ (\(P pos) -> P (pos + (b^.entity^.velocity) * (V2 dt dt) * (60 / 1000))) (b^.entity^.position)

updateBallRandomVelocity :: Ball -> IO Ball
updateBallRandomVelocity b = do
  x <- randomRIO (-10,1)
  y <- randomRIO (-20,20)
  let e = b^.entity & velocity %~ (+ V2 x y)
  return $ (b & entity .~ e)

updateBallVelocity :: V2 CFloat -> Ball -> IO Ball
updateBallVelocity grav b = do
  let e = b^.entity & velocity %~ (+ grav)
  return $ (b & entity .~ e)

collissionDetection :: World -> IO World
collissionDetection world = do
  b <- mapM ballHitScreen (world^.balls)
  return $ world & balls .~ b

frictionEntity :: CFloat -> Entity -> Entity
frictionEntity f e = e & velocity %~ (* V2 f f)

hitBottom :: Ball -> IO Ball
hitBottom ball = do
  let e = if getY (ball^.entity^.position) > windowHeight-(ball^.entity^.height) then (frictionEntity friction) $ repositionBottom $ ball^.entity & velocity %~ (* V2 1 (-1)) else ball^.entity
  return $ ball & entity .~ e

hitTop :: Ball -> IO Ball
hitTop ball = do
  let e = if getY (ball^.entity^.position) < 0 then frictionEntity friction $ repositionTop $ ball^.entity & velocity %~ (* V2 1 (-1)) else ball^.entity
  return $ ball & entity .~ e

hitRight :: Ball -> IO Ball
hitRight ball = do
  let e = if getX (ball^.entity^.position) > (windowWidth-(ball^.entity^.width)) then frictionEntity friction $ repositionRight $ ball^.entity & velocity %~ (* V2 (-1) 1) else ball^.entity
  return $ ball & entity .~ e

hitLeft :: Ball -> IO Ball
hitLeft ball = do
  let e = if getX (ball^.entity^.position) < 0 then frictionEntity friction $ repositionLeft $ ball^.entity & velocity %~ (* V2 (-1) 1) else ball^.entity
  return $ ball & entity .~ e

ballHitScreen :: Ball -> IO Ball
ballHitScreen b = hitBottom =<< hitTop =<< hitLeft =<< hitRight b

getY :: Point V2 CFloat -> CFloat
getY (P (V2 _ y )) =  y

getX :: Point V2 CFloat -> CFloat
getX (P (V2 x _ )) =  x

repositionBottom :: Entity -> Entity
repositionBottom e =
  e & position %~ (\ (P (V2 x _ )) -> P (V2 x (windowHeight-e^.height)))

repositionTop :: Entity -> Entity
repositionTop e =
  e & position %~ (\ (P (V2 x _ )) -> P (V2 x 0))

repositionRight :: Entity -> Entity
repositionRight e =
  e & position %~ (\ (P (V2 _ y)) -> P (V2 (windowWidth-e^.width) y))

repositionLeft :: Entity -> Entity
repositionLeft e =
  e & position %~ (\ (P (V2 _ y)) -> P (V2 0 y))

